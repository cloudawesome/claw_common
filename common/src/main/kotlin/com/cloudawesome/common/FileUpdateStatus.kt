package com.cloudawesome.common

enum class FileUpdateStatus(val value: Int) {
    Created(0), Updated(1), Deleted(2)
}