package com.cloudawesome.common

enum class ReplicationStrategy(val value: Int) {

    Naive(0), RandomSequence(1), RandomSequenceRandomSource(2), Claw(3), Clustering(4), Forward2N(5), Coded(6)
}