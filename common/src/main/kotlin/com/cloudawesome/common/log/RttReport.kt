package com.cloudawesome.common.log

data class RttReport (
    val entryId: Int,
    val rtt: Int
)