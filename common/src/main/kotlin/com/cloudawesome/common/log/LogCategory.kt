package com.cloudawesome.common.log

enum class LogCategory(val value: Int) {
    Initialization(0), Link(1), Finished(2), Sent(3), Received(4)
}