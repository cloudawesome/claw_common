package com.cloudawesome.common.log
import com.cloudawesome.common.FileUpdateStatus
import com.cloudawesome.common.ReplicationStrategy


data class LogEntry (
        val source: String,
        val destination: String,
        val endDestination: String,
        val event: FileUpdateStatus,
        val filename: String,
        val fileHash: String,
        val category: LogCategory,
        val replicationStrategy: ReplicationStrategy,
        val created: Long,
        val percentage: Double = 100.00
)