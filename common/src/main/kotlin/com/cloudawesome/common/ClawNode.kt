package com.cloudawesome.common

data class ClawNode(val id: Int, val name: String, val ip: String, val port: Int)